package com.wellsfargo.market.workshop.builder;

import java.util.Date;

import com.wellsfargo.market.workshop.domain.VIXTick;

public class VIXTickBuilder {

	private String symbol;
	private String open;
	private String high;
	private String low;
	private String tickpr; // Price
	private String volume;
	private String latestTradingDay;
	private String previousClose;
	private String change;
	private String changePercent;
	private String lambda;
	private Long epoch; // Current Timestamp
	private String tickvol; // Volatility
	
	public VIXTickBuilder withVolatility(String tickvol) {
		this.tickvol = tickvol;
		return this;
	}
	
	public VIXTickBuilder withTimeStamp(Long epoch) {
		this.epoch = epoch;
		return this;
	}
	
	public VIXTickBuilder withLambda(String lambda) {
		this.lambda = lambda;
		return this;
	}
	
	public VIXTickBuilder withSymbol(String symbol) {
		this.symbol = symbol;
		return this;
	}
	
	public VIXTickBuilder withOpen(String open) {
		this.open = open;
		return this;
	}
	
	public VIXTickBuilder withHigh(String high) {
		this.high = high;
		return this;
	}
	
	public VIXTickBuilder withLow(String low) {
		this.low = low;
		return this;
	}
	
	public VIXTickBuilder withPrice(String tickpr) {
		this.tickpr = tickpr;
		return this;
	}
	
	public VIXTickBuilder withVolume(String volume) {
		this.volume = volume;
		return this;
	}
	
	public VIXTickBuilder withLatestTradingDay(String latestTradingDay) {
		this.latestTradingDay = latestTradingDay;
		return this;
	}
	
	public VIXTickBuilder withPreviousClose(String previousClose) {
		this.previousClose = previousClose;
		return this;
	}
	
	public VIXTickBuilder withChange(String change) {
		this.change = change;
		return this;
	}
	
	public VIXTickBuilder withChangePercent(String changePercent) {
		this.changePercent = changePercent;
		return this;
	}
	
	public VIXTick build() {
		return new VIXTick(symbol, open, high, low, tickpr, volume, latestTradingDay, previousClose, change, changePercent, lambda, tickvol, epoch);
	}
}
