package com.wellsfargo.market.workshop.vix;

import com.wellsfargo.market.workshop.util.ObjectFactory;
import com.wellsfargo.market.workshop.util.PropertiesHelper;

public class OptionPricing {
	
	private static ObjectFactory objectFactory = new ObjectFactory();
	private static Object LOCK = new Object();
	
	public static void main(String a[]) throws Exception {
		PropertiesHelper.initializeProperties();
		
		VIXMarketDataProvider vixMarketDataProvider = objectFactory.getVIXMarketDataProvider();
		vixMarketDataProvider.getVIXDataAtScheduledInterval();
		
		// Save JVM from dying
		synchronized (LOCK) {
			LOCK.wait();
		}
	}
}
