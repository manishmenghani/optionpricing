package com.wellsfargo.market.workshop.vix;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimerTask;

import com.google.gson.Gson;
import com.wellsfargo.market.workshop.domain.VIXTick;
import com.wellsfargo.market.workshop.util.OptionPricingConstants;
import com.wellsfargo.market.workshop.util.OptionPricingUtil;
import com.wellsfargo.market.workshop.util.PropertiesHelper;
import com.wellsfargo.market.workshop.util.Publisher;

import static com.wellsfargo.market.workshop.builder.Builders.vixTickBuilder;

public class VIXMarketDataTask extends TimerTask {
	Publisher publisher ;
	public VIXMarketDataTask() {
		 publisher = new Publisher();
	}
	
	@Override
	public void run() {
		System.out.println("--- Going to get VIX data -----" + new Date());
		String marketDataRunningMode = PropertiesHelper.getProperty(OptionPricingConstants.VIX_MARKET_DATA_RUNNING_MODE);

		if(OptionPricingConstants.SIMULATOR.equalsIgnoreCase(marketDataRunningMode)) {
			List<String> ticksToPublish = new ArrayList<String>();
			String price = generateRandomPrice();
			String volatility = generateRandomVolatility();
			
			Date currentTimestamp = new Date();
			for(String lambda : OptionPricingUtil.LAMBDA_LIST) {
				VIXTick vixTick =  vixTickBuilder()
						.withPrice(price)
						.withLambda(lambda)
						.withTimeStamp(currentTimestamp.getTime())
						.withVolatility(volatility)
						.build();
				
				Gson gson = new Gson();
				String vixTickAsJson = gson.toJson(vixTick);
				ticksToPublish.add(vixTickAsJson);
			}
			
			publishToAWSTopic(ticksToPublish);
		}
	}

	private void publishToAWSTopic(List<String> ticksToPublish) {
		System.out.println("----------------------------------------------");
		/*for(String tick : ticksToPublish) {
			System.out.println(tick);
			publisher.publish(tick);
		}*/
		System.out.println(ticksToPublish.get(0));
		publisher.publish(ticksToPublish.get(0));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		publisher.publishToUI(ticksToPublish.get(0));
	}

	private String generateRandomPrice() { // TODO : Configure to change the format from config file
		Double max = Double.valueOf(PropertiesHelper.getProperty(OptionPricingConstants.PRICE_MAX_RANGE));
		Double min = Double.valueOf(PropertiesHelper.getProperty(OptionPricingConstants.PRICE_MIN_RANGE));
		
		double x = (Math.random() * ((max - min) + 1)) + min;
		double xrounded = Math.round(x * 100.0) / 100.0;
		return String.valueOf(xrounded);
	}
	
	private String generateRandomVolatility() { // TODO : Configure to change the format from config file
		Double max = Double.valueOf(PropertiesHelper.getProperty(OptionPricingConstants.VOLATILITY_MAX_RANGE));
		Double min = Double.valueOf(PropertiesHelper.getProperty(OptionPricingConstants.VOLATILITY_MIN_RANGE));
		
		double x = (Math.random() * ((max - min) + 1)) + min;
		double xrounded = Math.round(x * 100.0) / 100.0;
		return String.valueOf(xrounded);
	}

	private String getVIXTick() {
		StringBuilder vixData  = new StringBuilder();
		try {
			URL url = new URL("https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=VIX&interval=1min&apikey=6W0TKL8O1JQW0WUN");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
//				System.out.println(output);
				vixData.append(output);
			}
			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vixData.toString();
	}
}
