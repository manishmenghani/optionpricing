package com.wellsfargo.market.workshop.vix;

import java.util.Timer;
import java.util.TimerTask;

import com.wellsfargo.market.workshop.util.OptionPricingConstants;
import com.wellsfargo.market.workshop.util.PropertiesHelper;

public class VIXMarketDataProvider {

	// https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=VIX&interval=1min&apikey=6W0TKL8O1JQW0WUN
	
    public void getVIXDataAtScheduledInterval() {
    	TimerTask vixMarketDataTask = new VIXMarketDataTask();
    	//running timer task as daemon thread
        Timer timer = new Timer(true);
        // Third parameter is time between two executions --- Its in millisecond
        // 10*1000 = 10,000 milliseconds which is equivalent to 10 seconds
        String marketDataRunningMode = PropertiesHelper.getProperty(OptionPricingConstants.VIX_MARKET_DATA_RUNNING_MODE);
        System.out.println("--- Running system in SIMULATOR_MODE --- : "+OptionPricingConstants.SIMULATOR.equalsIgnoreCase(marketDataRunningMode));
        timer.scheduleAtFixedRate(vixMarketDataTask, 0, 10*1000);
        System.out.println("TimerTask started");
    }
}
