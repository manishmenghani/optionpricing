package com.wellsfargo.market.workshop.domain;

import java.util.Date;

public class VIXTick {

	// epoch : Timestamp
	//	tickpr : price
	// tickvol : volatility
	
	
	private String symbol;
	private String open;
	private String high;
	private String low;
	private String tickpr;
	private String volume;
	private String latestTradingDay;
	private String previousClose;
	private String change;
	private String changePercent;
	private String lambda;
	private Long epoch; // Current Timestamp
	private String tickvol; // Volatility
	
	public VIXTick() {
	}
	
	public VIXTick(String symbol, String open, String high, String low, String tickpr, String volume,
			String latestTradingDay, String previousClose, String change, String changePercent, String lambda, String tickvol, Long epoch) {
		this.symbol = symbol;
		this.open = open;
		this.high = high;
		this.low = low;
		this.tickpr = tickpr;
		this.volume = volume;
		this.latestTradingDay = latestTradingDay;
		this.previousClose = previousClose;
		this.change = change;
		this.changePercent = changePercent;
		this.lambda = lambda;
		this.tickvol = tickvol;
		this.epoch = epoch;
	}
	
	public String getLambda() {
		return lambda;
	}

	public void setLambda(String lambda) {
		this.lambda = lambda;
	}

	public String getSymbol() {
		return symbol;
	}
	
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	public String getOpen() {
		return open;
	}
	
	public void setOpen(String open) {
		this.open = open;
	}
	
	public String getHigh() {
		return high;
	}
	
	public void setHigh(String high) {
		this.high = high;
	}
	
	public String getLow() {
		return low;
	}
	
	public void setLow(String low) {
		this.low = low;
	}
	
	public String getTickpr() {
		return tickpr;
	}

	public void setTickpr(String tickpr) {
		this.tickpr = tickpr;
	}

	public Long getEpoch() {
		return epoch;
	}

	public void setEpoch(Long epoch) {
		this.epoch = epoch;
	}

	public String getTickvol() {
		return tickvol;
	}

	public void setTickvol(String tickvol) {
		this.tickvol = tickvol;
	}

	public String getVolume() {
		return volume;
	}
	
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	public String getLatestTradingDay() {
		return latestTradingDay;
	}
	
	public void setLatestTradingDay(String latestTradingDay) {
		this.latestTradingDay = latestTradingDay;
	}
	
	public String getPreviousClose() {
		return previousClose;
	}
	
	public void setPreviousClose(String previousClose) {
		this.previousClose = previousClose;
	}
	
	public String getChange() {
		return change;
	}
	
	public void setChange(String change) {
		this.change = change;
	}
	
	public String getChangePercent() {
		return changePercent;
	}
	
	public void setChangePercent(String changePercent) {
		this.changePercent = changePercent;
	}

	@Override
	public String toString() {
		return "VIXTick [symbol=" + symbol + ", open=" + open + ", high=" + high + ", low=" + low + ", tickpr=" + tickpr
				+ ", volume=" + volume + ", latestTradingDay=" + latestTradingDay + ", previousClose=" + previousClose
				+ ", change=" + change + ", changePercent=" + changePercent + ", lambda=" + lambda + ", epoch=" + epoch
				+ ", tickvol=" + tickvol + "]";
	}
}
