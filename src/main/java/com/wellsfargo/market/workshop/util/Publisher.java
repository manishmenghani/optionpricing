package com.wellsfargo.market.workshop.util;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

public class Publisher {

	private AmazonSNS snsClient;
	private final String ARNTopic = "arn:aws:sns:ap-south-1:073022239412:TickVolTopic";
	private final String ARNUITopic ="arn:aws:sns:ap-south-1:073022239412:TickUiTopic";
	final PublishRequest publishRequest;
	final PublishRequest publishRequestUI;
	
	public Publisher()
	{
		snsClient = AmazonSNSClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();
		publishRequest = new PublishRequest();
		publishRequest.setTopicArn(ARNTopic);
		publishRequestUI = new PublishRequest();
		publishRequestUI.setTopicArn(ARNUITopic);
	}
	
	public String publish(String message) {
		publishRequest.setMessage(message);
		PublishResult publishResponse = snsClient.publish(publishRequest);

		// Print the MessageId of the message.
		System.out.println("MessageId: " + publishResponse.getMessageId());
		return publishResponse.getMessageId();
		
	}
	
	public String publishToUI(String message) {
		publishRequestUI.setMessage(message);
		PublishResult publishResponse = snsClient.publish(publishRequestUI);

		// Print the MessageId of the message.
		System.out.println("MessageId: " + publishResponse.getMessageId());
		return publishResponse.getMessageId();
		
	}
}
