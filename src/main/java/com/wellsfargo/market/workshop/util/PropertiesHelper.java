package com.wellsfargo.market.workshop.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesHelper {

	private static Properties prop = new Properties();
	
	public static void initializeProperties(){
		InputStream input = null;

		try {
			input = PropertiesHelper.class.getClassLoader().getResourceAsStream("com/wellsfargo/market/workshop/properties/OptionPricing.properties");
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String getProperty(String key) {
		return prop.getProperty(key);
	}
}
