package com.wellsfargo.market.workshop.util;

public class OptionPricingConstants {

	public static final String PRICE_MAX_RANGE = "price_max_range";

	public static final String PRICE_MIN_RANGE = "price_min_range";
	
	public static final String VOLATILITY_MAX_RANGE = "volatility_max_range";

	public static final String VOLATILITY_MIN_RANGE = "volatility_min_range";

	public static String SIMULATOR = "simulator";
	
	public static String VIX_MARKET_DATA_RUNNING_MODE = "vix.market.data.running.mode";
}
