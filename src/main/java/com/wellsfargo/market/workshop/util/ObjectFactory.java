package com.wellsfargo.market.workshop.util;

import com.wellsfargo.market.workshop.vix.VIXMarketDataProvider;

public class ObjectFactory {

	public VIXMarketDataProvider getVIXMarketDataProvider() {
		VIXMarketDataProvider vixMarketDataProvider = new VIXMarketDataProvider();
		return vixMarketDataProvider;
	}
}
