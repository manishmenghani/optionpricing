package com.wellsfargo.market.workshop.util;

public class Test {
	
	public static void main(String args[]) {
		for(int i =0;i<100;i++) {
		       Double min = 10.0; //  Set To Your Desired Min Value
		        Double max = 15.0; //    Set To Your Desired Max Value
		        double x = (Math.random() * ((max - min) + 1)) + min; //    This Will Create 
//		        A Random Number Inbetween Your Min And Max.
		        double xrounded = Math.round(x * 100.0) / 100.0; // Creates Answer To 
//		        The Nearest 100 th, You Can Modify This To Change How It Rounds.
		        System.out.println(xrounded); //    This Will Now Print Out The 
//		        Rounded, Random Number.
		}
 
       }
}
