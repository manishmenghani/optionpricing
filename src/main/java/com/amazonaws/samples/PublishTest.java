package com.amazonaws.samples;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

public class PublishTest {
	
	public static void main(String[] args) {
		AmazonSNS snsClient = AmazonSNSClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();
		//AmazonSNS snsClient = snsClientBuilder.
		final PublishRequest publishRequest = new PublishRequest("arn:aws:sns:ap-south-1:073022239412:TickVolTopic", "Hello");
		final PublishResult publishResponse = snsClient.publish(publishRequest);

		// Print the MessageId of the message.
		System.out.println("MessageId: " + publishResponse.getMessageId());
	}

}

